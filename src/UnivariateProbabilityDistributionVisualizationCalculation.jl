# Copyright 2022 Neven Sajko. All rights reserved.

module UnivariateProbabilityDistributionVisualizationCalculation

using LinearAlgebra, MutableArithmetics

export kde, ecdf, hist, combined_distr_viz

# Scales nonnegative values in the vector v to the range of [0, 1].
scale(::Type{Out}, v::V) where
{Out <: Real, F <: Real, V <: AbstractVector{F}} =
  map(
    let m = maximum(v, init = 0)
      e -> Out(e) / Out(m)
    end,
    v)

struct Ones{T} <: AbstractVector{T}
  Ones(::Type{T}) where {T <: Any} = new{T}()
end
Base.eltype(::Type{Ones{T}}) where {T <: Any} = T
Base.getindex(S::Ones{T}, ::I) where {T <: Any, I <: Any} = one(T)

# KDE (with inverse bandwidth).
function kde_inv_bw(out_size::S,
                    inp::VIn,
                    inverse_bandwidth::R,
                    ::Type{MP} = BigFloat,
                    weights::VW = Ones(UInt8),
                    ::Type{OutY} = Float32,
                    grid::VR = range(first(inp), last(inp), out_size)) where
{S <: Signed,
 VIn <: AbstractVector{<: Real},
 R <: Real,
 MP <: AbstractFloat,
 OutY <: Real,
 VW <: AbstractVector{<: Real},
 VR <: AbstractVector{<: Real}}
  isempty(inp) && error("empty input")
  (out_size < 2) && error("grid too small")

  local sum = zero(MP)
  local tmp = zero(MP)
  local out = zeros(OutY, out_size)
  for j in 1:out_size
    sum = zero!!(sum)
    for i in eachindex(inp)
      local p = inp[i]

      tmp = zero!!(tmp)
      tmp = add!!(tmp, grid[j])
      tmp = sub!!(tmp, p)
      tmp = mul!!(tmp, inverse_bandwidth)
      tmp = mul!!(tmp, tmp)
      tmp = mul!!(tmp, -1/2)

      # exp not yet implemented in MutableArithmetics
      local e = exp(tmp)

      e = mul!!(e, weights[i])
      sum = add!!(sum, e)
    end
    out[j] = sum
  end

  grid, scale(OutY, out)
end

# Uses the standard normal probability density function as the kernel
# function.
#
# MP is the type used internally in this function for extra precision
# for more accurate evaluation.
#
# inp must be sorted.
kde(out_size::S,
    inp::VIn,
    bandwidth::R,
    ::Type{MP} = BigFloat,
    weights::VW = Ones(UInt8),
    ::Type{OutY} = Float32,
    grid::VR = range(first(inp), last(inp), out_size)) where
{S <: Signed,
 VIn <: AbstractVector{<: Real},
 R <: Real,
 MP <: AbstractFloat,
 OutY <: Real,
 VW <: AbstractVector{<: Real},
 VR <: AbstractVector{<: Real}} =
  kde_inv_bw(out_size, inp, 1 / bandwidth, MP, weights, OutY, grid)

# Empirical cumulative distribution function.
#
# inp must be sorted.
function ecdf(out_size::S,
              inp::VIn,
              weights::VW = Ones(UInt8),
              ::Type{OutY} = Float32,
              grid::VR = range(first(inp), last(inp), out_size)) where
{S <: Signed,
 VIn <: AbstractVector{<: Real},
 OutY <: Real,
 VW <: AbstractVector{<: Real},
 VR <: AbstractVector{<: Real}}
  isempty(inp) && error("empty input")
  (out_size < 2) && error("grid too small")

  local counts = zeros(Int, out_size)
  local g = 1
  for i in eachindex(inp)
    local p = inp[i]
    while grid[g] < p
      counts[g + 1] = counts[g]
      g += 1
    end
    counts[g] += weights[i]
  end

  grid, scale(OutY, counts)
end

# Histogram.
#
# inp must be sorted.
function hist(bin_count::S,
              inp::VIn,
              weights::VW = Ones(UInt8),
              ::Type{OutY} = Float32,
              grid::VR = range(first(inp), last(inp), bin_count + 1)) where
{S <: Signed,
 VIn <: AbstractVector{<: Real},
 OutY <: Real,
 VW <: AbstractVector{<: Real},
 VR <: AbstractVector{<: Real}}
  isempty(inp) && error("empty input")
  (bin_count < 1) && error("grid too small")

  local counts = zeros(Int, bin_count)
  local g = 1
  for i in eachindex(inp)
    local p = inp[i]
    while grid[g + 1] < p
      g += 1
    end
    counts[g] += weights[i]
  end

  local x = 1 / (2*bin_count)
  local y = (1 - x, x)
  local z = (first(inp), last(inp))
  range(y ⋅ z, reverse(y) ⋅ z, bin_count), scale(OutY, counts)
end

# Returns a four-tuple:
#
#  * a grid (for the KDE and ECDF)
#
#  * an n-tuple with an element for each bandwidth value:
#
#    * a vector of density values (scaled to the range of [0, 1]), one
#      for each grid value
#
#  * a vector of cumulative probability values, one for each grid value
#
#  * a pair:
#
#    * a grid of histogram bin centers
#
#    * a vector of histogram values (scaled to the range of [0, 1]) for
#      each bin
combined_distr_viz(
  resolution::S1,
  bin_count::S2,
  inp::VIn,
  bws::NTuple{n, R},
  ::Type{KDEMP} = BigFloat,
  weights::VW = Ones(UInt8),
  ::Type{OutY} = Float32,
  grid::VR = range(first(inp), last(inp), resolution)) where
{S1 <: Signed,
 S2 <: Signed,
 VIn <: AbstractVector{<: Real},
 n,
 R <: Real,
 KDEMP <: AbstractFloat,
 OutY <: Real,
 VW <: AbstractVector{<: Real},
 VR <: AbstractVector{<: Real}} =
  grid,
  ntuple(
    let r = resolution, d = inp, bws = bws, wght = weights, grid = grid
      i -> kde(r, d, bws[i], KDEMP, wght, OutY, grid)[2]
    end,
    Val(n)),
  ecdf(resolution, inp, weights, OutY, grid)[2],
  hist(bin_count, inp, weights, OutY)

# Returns a four-tuple:
#
#  * a grid (for the KDE and ECDF)
#
#  * a vector of density values (scaled to the range of [0, 1]), one
#    for each grid value
#
#  * a vector of cumulative probability values, one for each grid value
#
#  * a pair:
#
#    * a grid of histogram bin centers
#
#    * a vector of histogram values (scaled to the range of [0, 1]) for
#      each bin
function combined_distr_viz(
  resolution::S1,
  bin_count::S2,
  inp::VIn,
  bw::R,
  ::Type{KDEMP} = BigFloat,
  weights::VW = Ones(UInt8),
  ::Type{OutY} = Float32,
  grid::VR = range(first(inp), last(inp), resolution)) where
{S1 <: Signed,
 S2 <: Signed,
 VIn <: AbstractVector{<: Real},
 R <: Real,
 KDEMP <: AbstractFloat,
 OutY <: Real,
 VW <: AbstractVector{<: Real},
 VR <: AbstractVector{<: Real}}
  local _, (k,), e, h = combined_distr_viz(
    resolution,
    bin_count,
    inp,
    (bw,),
    KDEMP,
    weights,
    OutY,
    grid)
  grid, k, e, h
end

end  # module UnivariateProbabilityDistributionVisualizationCalculation
