# UnivariateProbabilityDistributionVisualizationCalculation

Implements several complementary methods for visualizing estimations of
univariate probability distributions from samples: kernel density
estimation (KDE), empirical (cumulative) distribution function (ECDF),
and histograms.

The combined_distr_viz function invokes all three of those and returns
a tuple of their results.

The intended usage is displaying all three on the same plot. Thus all
output values are scaled to the range [0, 1].

The algorithms are all naive (so there are no FFT-caused wrap-around
issues for KDE), but the KDE function is designed for efficient
evaluation in multiple precision (with, e.g., `BigFloat`).

The functions take sorted input data.

## About the KDE function, `kde`

KDE is a continuous, and more useful, variation on the histogram. The
bandwidth parameter (a real number) influences the shape of the KDE
curve to a great extent. It is analogous to the bin size of a
histogram. Values of lesser magnitude emphasize local features of the
data distribution density, while greater bandwidth values make the
density curve smoother, emphasizing large-scale properties.

So what bandwidth one wants depends both on the distribution and on
what exactly one is interested in.

## Usage example

Some example Gadfly.jl plots.

```
using
  MultiFloats,  # for KDE, faster than BigFloat
  Gadfly,
  RDatasets,
  UnivariateProbabilityDistributionVisualizationCalculation

import Cairo, Fontconfig  # for Gadfly

# Note: use SVGJS instead of PNG for interactivity: panning, zooming,
# etc.
d(name, plot) = draw(PNG(name * ".png", 20cm, 18cm, dpi = 150), plot)

grid, dat_kde, dat_ecdf, (grid_hist, dat_hist) = combined_distr_viz(
  2^11,
  12,
  sort!(dataset("Ecdat", "VietNamI").Illdays),
  0.5,
  Float64x6)
```

### KDE

```
d("KDE distr plot",
  plot(x = grid, y = dat_kde, Geom.line))
```

![KDE](img/KDE%20distr%20plot.png "KDE")

### ECDF

```
d("ECDF distr plot",
  plot(x = grid, y = dat_ecdf, Geom.line))
```

![ECDF](img/ECDF%20distr%20plot.png "ECDF")

### Histogram

```
d("histogram distr plot",
  plot(x = grid_hist, y = dat_hist, Geom.bar))
```

![histogram](img/histogram%20distr%20plot.png "histogram")

### All three

```
d("combined distr plot",
  plot(
    Guide.title("Visualization of the Distribution of the Number of Illness Days"),
    Guide.xlabel("respondent's number of illness days in that year"),
    Guide.ylabel("distribution of the number of illness days"),
    layer(x = grid,      y = dat_kde,  color = ["kde"],  Geom.line),
    layer(x = grid,      y = dat_ecdf, color = ["ecdf"], Geom.line),
    layer(x = grid_hist, y = dat_hist, color = ["hist"], Geom.bar)))
```

![combined](img/combined%20distr%20plot.png "combined")

## Usage example - with multiplicities/weights

The functions exported by this package optionally take an additional
vector, each element representing the multiplicity/weight of the
corresponding element of the main input data vector. It defaults to a
vector with every element equal to one.

Setting an multiplicity to zero effectively removes the corresponding
element from the main input data vector, while setting it to, e.g., 3
makes the corresponding element be triple-counted.

So setting a weight to a positive integer is equivalent to just
repeating the main input data element that many times.

In this example we've got a table with extramarital affair data with
two columns in particular that we're interested in: age of the
respondent, and the number of extramarital affairs in that year. We can
take the age column as our main input vector and the latter column can
then be our weights vector. We just need to be careful to reorder the
weights using the same permutation used for sorting the ages. This is
what `sortperm` is for.

So the ages of the respondents weighted by the number of affairs can
basically tell us something like at which age most affairs happen.

This time we're plotting two KDEs, each with a different bandwidth.

```
data = dataset("Ecdat", "Fair").Age
weights = dataset("Ecdat", "Fair").NBAffairs
perm = sortperm(data)

# Bandwidths for KDE.
bws = (1.0, 4.0)

grid, dat_kde, dat_ecdf, (grid_hist, dat_hist) = combined_distr_viz(
  2^11,
  9,
  data[perm],
  bws,
  Float64x6,
  weights[perm])

d("combined distr plot - Fair's affairs",
  plot(
    Guide.title("Visualization of Extramarital Affairs by Age"),
    Guide.xlabel("respondent age"),
    Guide.ylabel("distribution of respondent age weighted by number of affairs in that year"),
    map(
      let g = grid
        (d, bw) -> layer(x = g,
                         y = d,
                         color = [string("kde with bw=", bw)],
                         Geom.line)
      end,
      dat_kde,
      bws)...,
    layer(x = grid,      y = dat_ecdf, color = ["ecdf"], Geom.line),
    layer(x = grid_hist, y = dat_hist, color = ["hist"], Geom.bar)))
```

![combined2](img/combined%20distr%20plot%20-%20Fair's%20affairs.png "combined2")

One of the KDEs, with bandwidth equal to one, displays a regular
pattern, a result of the poll's design (allowed ages are restricted to
only some, equally-spaced, values). Since we know about that, it makes
sense to "oversmooth" the density plot by using the other bandwidth
value.

## Issues

TODO: implement binned KDE?
